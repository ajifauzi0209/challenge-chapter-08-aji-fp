package com.kazakimaru.ch08_ajifauzipangestu.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kazakimaru.ch08_ajifauzipangestu.repository.UserRepo
import kotlinx.coroutines.launch

class LoginViewModel(private val userRepo: UserRepo): ViewModel() {

    val statusLogin = MutableLiveData<Boolean>()
    val username = MutableLiveData<String>()

    fun loginUser(email: String, password: String) {
        viewModelScope.launch {
            val checkUser = userRepo.checkRegisteredUser(email, password)

            // Jika email dan password ditemukan pada db
            if (!checkUser.isNullOrEmpty()) {
                // Mendapatkan data user dari inputan login
                val getUser = userRepo.getUsernameByMail(email)
                username.value = getUser?.username
                // Login State = True
                statusLogin.value = true
            } else {
                statusLogin.value = false
            }
        }
    }

}