package com.kazakimaru.ch08_ajifauzipangestu.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kazakimaru.ch08_ajifauzipangestu.database.User
import com.kazakimaru.ch08_ajifauzipangestu.repository.UserRepo
import kotlinx.coroutines.launch

class RegisterViewModel(private val userRepo: UserRepo): ViewModel() {

    val statusRegister = MutableLiveData<Boolean>()
    val emailHasRegistered = MutableLiveData<Boolean>()
    val usernameHasRegistered = MutableLiveData<Boolean>()

    fun addUserToDb(username: String, email: String, user: User) {
        var result1 = false
        var result2 = false

        viewModelScope.launch {
            // Query check username & email
            val checkUsername = userRepo.checkRegisteredkUsername(username)
            val checkEmail = userRepo.checkRegisteredEmail(email)

            if (!checkUsername.isNullOrEmpty()) { // jika ditemukan username sudah dipakai
                usernameHasRegistered.value = true
            } else {
                result1 = true
            }

            if (!checkEmail.isNullOrEmpty()) { // jika ditemukan email sudah dipakai
                emailHasRegistered.value = true
            } else {
                result2 = true
            }

            if (result1 && result2) { // Jika username & email tersedia
                statusRegister.value = true
                // Jalankan query insert to db
                userRepo.insertUser(user)
            } else {
                statusRegister.value = false
            }
        }
    }

}