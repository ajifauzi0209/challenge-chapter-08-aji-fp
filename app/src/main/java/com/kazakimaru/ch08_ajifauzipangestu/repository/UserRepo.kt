package com.kazakimaru.ch08_ajifauzipangestu.repository

import android.content.Context
import com.kazakimaru.ch08_ajifauzipangestu.database.User
import com.kazakimaru.ch08_ajifauzipangestu.database.UserDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class UserRepo(context: Context) {
    private val userDB = UserDatabase.getInstance(context)

    suspend fun checkRegisteredkUsername(username: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.checkRegisteredUsername(username)
    }

    suspend fun checkRegisteredEmail(email: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.checkRegisteredEmail(email)
    }

    suspend fun checkRegisteredUser(email: String, password: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.checkRegisteredUser(email, password)
    }

    suspend fun getUsernameByMail(email: String) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.getUsernameByEmail(email)
    }

    suspend fun insertUser(user: User) = withContext(Dispatchers.IO) {
        userDB?.userDao()?.insertUser(user)
    }

}