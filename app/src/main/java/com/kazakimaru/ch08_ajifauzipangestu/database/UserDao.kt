package com.kazakimaru.ch08_ajifauzipangestu.database

import androidx.room.*

@Dao
interface UserDao {
    // Cek ketersediaan username pada saat register
    @Query("SELECT * FROM User WHERE username = :username")
    fun checkRegisteredUsername(username: String): List<User>

    // Cek ketersediaan email pada saat register
    @Query("SELECT * FROM User WHERE email = :email")
    fun checkRegisteredEmail(email: String): List<User>

    // Cek ketersediaan email and password pada saat login
    @Query("SELECT * FROM User WHERE email = :email AND password = :password")
    fun checkRegisteredUser(email: String, password: String): List<User>

    // Mendapatkan username berdasarkan email login
    @Query("SELECT * FROM User WHERE email = :email")
    fun getUsernameByEmail(email: String): User

    // Insert ke Database
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: User): Long

}