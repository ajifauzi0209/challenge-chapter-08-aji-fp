package com.kazakimaru.ch08_ajifauzipangestu.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.kazakimaru.ch08_ajifauzipangestu.R
import com.kazakimaru.ch08_ajifauzipangestu.database.User
import com.kazakimaru.ch08_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch08_ajifauzipangestu.repository.UserRepo
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.RegisterViewModel

class RegisterActivity : AppCompatActivity() {
    private val userRepo: UserRepo by lazy { UserRepo(this) }
    private val registerViewModel: RegisterViewModel by viewModelsFactory { RegisterViewModel(userRepo) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { RegisterInterface() }
        observeData()
    }

    @Preview(showSystemUi = true)
    @Composable
    private fun RegisterInterface() {
        val plusJakartaSansRegular = FontFamily(Font(R.font.plusjakartasansregular))
        val plusJakartaSansBold = FontFamily(Font(R.font.plusjakartasansbold))

        Column(
            modifier = Modifier.padding(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            // Start Title
            Text(
                text = "Register",
                color = Color.Black,
                fontSize = 24.sp,
                fontFamily = plusJakartaSansBold
            )
            // End Title

            Spacer(modifier = Modifier.height(32.dp))

            // Start Logo Image
            Image(
                painter = painterResource(id = R.drawable.binar_logo),
                contentDescription = "Logo Binar",
                modifier = Modifier
                    .fillMaxWidth()
                    .size(width = 0.dp, height = 140.dp)
            )
            // End Logo Image

            Spacer(modifier = Modifier.height(32.dp))

            // Start TextField Username
            var textUsername by remember { mutableStateOf("") }
            TextField(
                value = textUsername,
                onValueChange = { textUsername = it },
                label = { Text(text = "Username") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                shape = RoundedCornerShape(8.dp),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.purple_binar),
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                textStyle = TextStyle(
                    fontFamily = plusJakartaSansRegular
                )
            )
            // End TextField Username

            Spacer(modifier = Modifier.height(16.dp))

            // Start TextField Email
            var textEmail by remember { mutableStateOf("") }
            TextField(
                value = textEmail,
                onValueChange = { textEmail = it },
                label = { Text(text = "Email") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email
                ),
                shape = RoundedCornerShape(8.dp),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.purple_binar),
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                textStyle = TextStyle(
                    fontFamily = plusJakartaSansRegular
                )
            )
            // End TextField Email

            Spacer(modifier = Modifier.height(16.dp))

            // Start TextField Password
            var textPassword by remember { mutableStateOf("") }
            var passVisibility by remember { mutableStateOf(false) }

            val icon = if (passVisibility) {
                painterResource(id = R.drawable.ic_baseline_visibility_off_24)
            } else {
                painterResource(id = R.drawable.ic_baseline_visibility_24)
            }
            TextField(
                value = textPassword,
                onValueChange = { textPassword = it },
                label = { Text(text = "Password") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                shape = RoundedCornerShape(8.dp),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.purple_binar),
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                trailingIcon = {
                    IconButton(onClick = {
                        passVisibility = !passVisibility
                    }) {
                        Icon(
                            painter = icon,
                            contentDescription = "visibility"
                        )
                    }
                },
                textStyle = TextStyle(
                    fontFamily = plusJakartaSansRegular
                ),
                visualTransformation = if (passVisibility) {
                    VisualTransformation.None
                } else {
                    PasswordVisualTransformation()
                }
            )
            // End TextField Password

            Spacer(modifier = Modifier.height(16.dp))

            // Start TextField Confirm Password
            var textConfirmPassword by remember { mutableStateOf("") }
            var passConfirmVisibility by remember { mutableStateOf(false) }

            val iconConfirm = if (passConfirmVisibility) {
                painterResource(id = R.drawable.ic_baseline_visibility_off_24)
            } else {
                painterResource(id = R.drawable.ic_baseline_visibility_24)
            }
            TextField(
                value = textConfirmPassword,
                onValueChange = { textConfirmPassword = it },
                label = { Text(text = "Konfirmasi Password") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                shape = RoundedCornerShape(8.dp),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.purple_binar),
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                trailingIcon = {
                    IconButton(onClick = {
                        passConfirmVisibility = !passConfirmVisibility
                    }) {
                        Icon(
                            painter = iconConfirm,
                            contentDescription = "visibility"
                        )
                    }
                },
                textStyle = TextStyle(
                    fontFamily = plusJakartaSansRegular
                ),
                visualTransformation = if (passConfirmVisibility) {
                    VisualTransformation.None
                } else {
                    PasswordVisualTransformation()
                }
            )
            // End TextField Confirm Password

            Spacer(modifier = Modifier.height(32.dp))

            // Start Button Register
            Button(
                onClick = {
                    doRegister(textUsername, textEmail, textPassword, textConfirmPassword)
                },
                modifier = Modifier.fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(
                    colorResource(id = R.color.purple_binar)
                ),
                shape = RoundedCornerShape(8.dp),
                contentPadding = PaddingValues(
                    start = 20.dp,
                    end = 20.dp,
                    top = 16.dp,
                    bottom = 16.dp
                )
            ) {
                Text(
                    text = "Register",
                    color = Color.White,
                    fontSize = 18.sp,
                    fontFamily = plusJakartaSansBold
                )
            }
            // End Button Register

        }
    }

    private fun doRegister(username: String, email: String, pass1: String, pass2: String) {
        // Validasi inputan jika tidak ada yg kosong / jml karakter terpenuhi
        if (registerValidation(username, email, pass1, pass2)) {
            val user = User(null, username, email, pass1)
            // Jalankan fungsi addUserToDb di RegisterViewModel
            registerViewModel.addUserToDb(username, email, user)
        }
    }

    private fun registerValidation(username: String, email: String, pass1: String, pass2: String): Boolean {
        var result = true
        if (username.isEmpty()) { // jika kosong
            Toast.makeText(this, "Username tidak boleh kosong!", Toast.LENGTH_SHORT).show()
            result = false
        } else if (username.length < 5) { // jika kurang dari 5 karakter
            Toast.makeText(this, "Minimum 5 karakter!", Toast.LENGTH_SHORT).show()
            result = false
        }

        if (email.isEmpty()) { // jika kosong
            Toast.makeText(this, "Email tidak boleh kosong!", Toast.LENGTH_SHORT).show()
            result = false
        }

        if (pass1.isEmpty()) { // jika kosong
            Toast.makeText(this, "Password tidak boleh kosong!", Toast.LENGTH_SHORT).show()
            result = false
        } else if (pass1.length < 6) { // jika kurang dari 6 karakter
            Toast.makeText(this, "Password minimum 6 Karakter!", Toast.LENGTH_SHORT).show()
            result = false
        }

        if (pass2.isEmpty()) { // jika kosong
            Toast.makeText(this, "Konfirmasi Password tidak boleh kosong!", Toast.LENGTH_SHORT).show()
            result = false
        } else if (pass2 != pass1) { // jika konfirm pass tdk sama dgn pass
            Toast.makeText(this, "Password harus sama!", Toast.LENGTH_SHORT).show()
            result = false
        }

        return result
    }

    private fun moveToLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun observeData() {
        registerViewModel.usernameHasRegistered.observe(this) {
            showToast("Username sudah dipakai")
        }

        registerViewModel.emailHasRegistered.observe(this) {
            showToast("Email sudah dipakai")
        }

        registerViewModel.statusRegister.observe(this) {
            if (it) {
                moveToLogin()
                showToast("Berhasil Daftar")
            } else {
                showToast("Gagal Daftar")
            }
        }
    }

}