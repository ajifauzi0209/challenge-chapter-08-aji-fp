package com.kazakimaru.ch08_ajifauzipangestu.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.tooling.preview.Preview
import com.kazakimaru.ch08_ajifauzipangestu.R
import com.kazakimaru.ch08_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch08_ajifauzipangestu.repository.DatastoreManager
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.DatastoreViewModel

class SplashActivity : AppCompatActivity() {
    private lateinit var handler: Handler

    private val pref: DatastoreManager by lazy { DatastoreManager(this) }
    private val datastoreViewModel: DatastoreViewModel by viewModelsFactory { DatastoreViewModel(pref) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { SplashInterface() }
        checkLoginState()
    }

    @Preview(showSystemUi = true)
    @Composable
    private fun SplashInterface() {
        Column(
            modifier = Modifier.fillMaxSize(),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Image(
                painter = painterResource(id = R.drawable.binar_logo),
                contentDescription = "Logo Binar",
            )
        }
    }

    private fun checkLoginState() {
        datastoreViewModel.getLoginState().observe(this) {
            if (it) {
                moveToHome()
            } else {
                moveToLogin()
            }
        }
    }

    private fun moveToLogin() {
        handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            startActivity(Intent(this, LoginActivity::class.java))
            finish()
        }, 3000)
    }

    private fun moveToHome() {
        handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            startActivity(Intent(this, HomeActivity::class.java))
            finish()
        }, 3000)
    }
}