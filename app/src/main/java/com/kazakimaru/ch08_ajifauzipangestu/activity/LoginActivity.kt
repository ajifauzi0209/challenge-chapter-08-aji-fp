package com.kazakimaru.ch08_ajifauzipangestu.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.text.input.PasswordVisualTransformation
import androidx.compose.ui.text.input.VisualTransformation
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.kazakimaru.ch08_ajifauzipangestu.R
import com.kazakimaru.ch08_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch08_ajifauzipangestu.repository.DatastoreManager
import com.kazakimaru.ch08_ajifauzipangestu.repository.UserRepo
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.DatastoreViewModel
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.LoginViewModel

class LoginActivity : AppCompatActivity() {
    private val userRepo: UserRepo by lazy { UserRepo(this) }
    private val loginViewModel: LoginViewModel by viewModelsFactory { LoginViewModel(userRepo) }

    private val pref: DatastoreManager by lazy { DatastoreManager(this) }
    private val datastoreViewModel: DatastoreViewModel by viewModelsFactory { DatastoreViewModel(pref) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { LoginInterface() }
        observeData()
    }

    @Preview(showSystemUi = true)
    @Composable
    private fun LoginInterface() {
        val plusJakartaSansRegular = FontFamily(Font(R.font.plusjakartasansregular))
        val plusJakartaSansBold = FontFamily(Font(R.font.plusjakartasansbold))
        Column(
            modifier = Modifier.padding(32.dp),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            // Start Title
            Text(
                text = "Login",
                color = Color.Black,
                fontSize = 24.sp,
                fontFamily = plusJakartaSansBold
            )
            // End Title

            Spacer(modifier = Modifier.height(32.dp))

            // Start Logo Image
            Image(
                painter = painterResource(id = R.drawable.binar_logo),
                contentDescription = "Logo Binar",
                modifier = Modifier
                    .fillMaxWidth()
                    .size(width = 0.dp, height = 140.dp)
            )
            // End Logo Image

            Spacer(modifier = Modifier.height(32.dp))

            // Start TextField Email
            var textEmail by remember { mutableStateOf("") }
            TextField(
                value = textEmail,
                onValueChange = { textEmail = it },
                label = { Text(text = "Email") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Email
                ),
                shape = RoundedCornerShape(8.dp),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.purple_binar),
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                textStyle = TextStyle(
                    fontFamily = plusJakartaSansRegular
                )
            )
            // End TextField Email

            Spacer(modifier = Modifier.height(16.dp))

            // Start TextField Password
            var textPassword by remember { mutableStateOf("") }
            var passVisibility by remember { mutableStateOf(false) }

            val icon = if (passVisibility) {
                painterResource(id = R.drawable.ic_baseline_visibility_off_24)
            } else {
                painterResource(id = R.drawable.ic_baseline_visibility_24)
            }
            TextField(
                value = textPassword,
                onValueChange = { textPassword = it },
                label = { Text(text = "Password") },
                modifier = Modifier.fillMaxWidth(),
                singleLine = true,
                keyboardOptions = KeyboardOptions(
                    keyboardType = KeyboardType.Password
                ),
                shape = RoundedCornerShape(8.dp),
                colors = TextFieldDefaults.textFieldColors(
                    cursorColor = colorResource(id = R.color.purple_binar),
                    focusedIndicatorColor = Color.Transparent,
                    unfocusedIndicatorColor = Color.Transparent,
                ),
                trailingIcon = {
                    IconButton(onClick = {
                        passVisibility = !passVisibility
                    }) {
                        Icon(
                            painter = icon,
                            contentDescription = "visibility"
                        )
                    }
                },
                textStyle = TextStyle(
                    fontFamily = plusJakartaSansRegular
                ),
                visualTransformation = if (passVisibility) {
                    VisualTransformation.None
                } else {
                    PasswordVisualTransformation()
                }
            )
            // End TextField Password

            Spacer(modifier = Modifier.height(32.dp))

            // Start Button Login
            Button(
                onClick = { doLogin(textEmail, textPassword) },
                modifier = Modifier.fillMaxWidth(),
                colors = ButtonDefaults.buttonColors(
                    colorResource(id = R.color.purple_binar)
                ),
                shape = RoundedCornerShape(8.dp),
                contentPadding = PaddingValues(
                    start = 20.dp,
                    end = 20.dp,
                    top = 16.dp,
                    bottom = 16.dp
                )
            ) {
                Text(
                    text = "Login",
                    color = Color.White,
                    fontSize = 18.sp,
                    fontFamily = plusJakartaSansBold
                )
            }
            // End Button Login

            Spacer(modifier = Modifier.height(16.dp))

            // Start Daftar
            ClickableText(
                text = AnnotatedString("Belum punya akun? Daftar"),
                style = TextStyle(
                    textAlign = TextAlign.Center
                ),
                onClick = { moveToRegister() }
            )
            // End Daftar

        }
    }

    private fun doLogin(email: String, password: String) {
        if (loginValidation(email, password)) {
            loginViewModel.loginUser(email, password)
        }
    }

    private fun loginValidation(email: String, password: String): Boolean {
        var result = true
        if (email.isEmpty()) { // jika kosong
            showToast("Email tidak boleh kosong!")
            result = false
        }

        if (password.isEmpty()) { // jika kosong
            showToast("Password tidak boleh kosong!")
            result = false
        }

        return result
    }

    private fun moveToRegister() {
        startActivity(Intent(this, RegisterActivity::class.java))
    }

    private fun moveToHome() {
        startActivity(Intent(this, HomeActivity::class.java))
        finish()
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    private fun observeData() {
        loginViewModel.statusLogin.observe(this) {
            if (it) {
                // Simpan Login State ke DataStore
                datastoreViewModel.saveLoginState(it) // True
                // Munculkan Toast
                showToast("Berhasil Login")
                // Pindah screen ke HomeActivity
                moveToHome()
            } else {
                showToast("Email atau Password Salah!")
            }
        }

        loginViewModel.username.observe(this) {
            // Simpan Username ke DataStore
            datastoreViewModel.saveUsername(it)
        }
    }
}