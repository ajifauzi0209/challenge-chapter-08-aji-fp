package com.kazakimaru.ch08_ajifauzipangestu.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.foundation.text.ClickableText
import androidx.compose.material.Card
import androidx.compose.material.CircularProgressIndicator
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.text.AnnotatedString
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.kazakimaru.ch08_ajifauzipangestu.BuildConfig
import com.kazakimaru.ch08_ajifauzipangestu.R
import com.kazakimaru.ch08_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch08_ajifauzipangestu.repository.DatastoreManager
import com.kazakimaru.ch08_ajifauzipangestu.service.TMDBApiService
import com.kazakimaru.ch08_ajifauzipangestu.service.TMDBClient
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.DatastoreViewModel
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.TMDBViewModel

class HomeActivity : AppCompatActivity() {
    private val pref: DatastoreManager by lazy { DatastoreManager(this) }
    private val datastoreViewModel: DatastoreViewModel by viewModelsFactory { DatastoreViewModel(pref) }

    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val tmdbViewModel: TMDBViewModel by viewModelsFactory { TMDBViewModel(apiService) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { HomeInterface() }

        tmdbViewModel.getAllMoviePopular()
    }

    @Composable
    private fun HomeInterface() {
        // Font
        val plusJakartaSansRegular = FontFamily(Font(R.font.plusjakartasansregular))
        val plusJakartaSansBold = FontFamily(Font(R.font.plusjakartasansbold))

        // Get Username
        val myUsername by datastoreViewModel.getUsername().observeAsState()

        // Get Movie from API
        val myMovieList by tmdbViewModel.dataSuccess.observeAsState()
        val getAllData = myMovieList?.results

        Column {
            // Start Row Username & Logout
            Row() {
                // Start Box Text Username
                Box(
                    modifier = Modifier
                        .padding(top = 16.dp, start = 32.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    contentAlignment = Alignment.CenterStart
                ) {
                    Text(
                        text = "Welcome, $myUsername",
                        color = Color.Black,
                        fontSize = 16.sp,
                        fontFamily = plusJakartaSansRegular
                    )
                }
                // End Box Text Username

                // Start Box Text Logout
                Box(
                    modifier = Modifier
                        .padding(top = 16.dp, end = 32.dp)
                        .fillMaxWidth()
                        .weight(1f),
                    contentAlignment = Alignment.CenterEnd
                ) {
                    ClickableText(
                        text = AnnotatedString("Logout"),
                        style = TextStyle(
                            textAlign = TextAlign.Center,
                            color = Color.Black,
                            fontSize = 16.sp,
                            fontFamily = plusJakartaSansBold
                        ),
                        onClick = { doLogout() }
                    )
                }
                // End Box Text Logout
            }
            // End Row Username & Logout

            Spacer(modifier = Modifier.height(24.dp))

            // Start Title Home
            Text(
                text = "Home",
                color = Color.Black,
                fontSize = 20.sp,
                fontFamily = plusJakartaSansBold,
                modifier = Modifier.fillMaxWidth(),
                textAlign = TextAlign.Center
            )
            // End Title Home

            Spacer(modifier = Modifier.height(16.dp))

            // Start LazyColumn Movie
            LazyColumn() {
                if (getAllData.isNullOrEmpty()) {
                    item {
                        CircularProgressIndicator(
                            modifier = Modifier
                                .fillMaxSize()
                                .wrapContentSize(align = Alignment.Center)
                        )
                    }
                } else {
                    items(items = getAllData) { result ->
                        Card(
                            shape = RoundedCornerShape(8.dp),
                            border = BorderStroke(width = 2.dp, color = colorResource(id = R.color.border_card)),
                            modifier = Modifier
                                .fillMaxWidth()
                                .padding(horizontal = 16.dp, vertical = 8.dp)
                                .clickable {
                                    val intent =
                                        Intent(this@HomeActivity, MovieDetailActivity::class.java)
                                    val bundle = Bundle()

                                    bundle.putInt(DATABUNDLE, result.id)
                                    intent.putExtra(BUNDLEEXTRA, bundle)
                                    startActivity(intent)
                                }
                        ) {
                            Row(
                                modifier = Modifier.fillMaxWidth(),
                                horizontalArrangement = Arrangement.spacedBy(16.dp)
                            ) {
                                // Start Image Poster
                                AsyncImage(
                                    model = BuildConfig.BASE_URL_IMAGE + result.posterPath,
                                    contentDescription = null,
                                    modifier = Modifier.size(width = 120.dp, height = 150.dp),
                                    contentScale = ContentScale.Crop
                                )
                                // End Image Poster

                                Column(
                                    modifier = Modifier
                                        .fillMaxHeight()
                                        .padding(end = 12.dp, top = 4.dp),
                                    verticalArrangement = Arrangement.spacedBy(6.dp)
                                ) {
                                    // Start Title Movie
                                    Text(
                                        text = result.title,
                                        color = Color.Black,
                                        fontFamily = plusJakartaSansBold,
                                        fontSize = 18.sp,
                                        maxLines = 1,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                    // End Image Poster

                                    // Start Overview
                                    Text(
                                        text = result.overview,
                                        color = Color.Black,
                                        fontFamily = plusJakartaSansRegular,
                                        fontSize = 16.sp,
                                        maxLines = 4,
                                        overflow = TextOverflow.Ellipsis
                                    )
                                    // End Overview
                                }
                            }
                        }
                    }
                }
            }
            // End LazyColumn Movie

        }
    }

    private fun doLogout() {
        datastoreViewModel.deleteAllData()
        showToast("Berhasil Logout")
        moveToLogin()
    }

    private fun moveToLogin() {
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
    }

    private fun showToast(text: String) {
        Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val DATABUNDLE = "databundle"
        const val BUNDLEEXTRA = "bundleextra"
    }

}