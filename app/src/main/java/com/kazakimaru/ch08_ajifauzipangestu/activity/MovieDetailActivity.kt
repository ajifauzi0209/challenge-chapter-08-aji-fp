package com.kazakimaru.ch08_ajifauzipangestu.activity

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.foundation.layout.*
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import coil.compose.AsyncImage
import com.kazakimaru.ch08_ajifauzipangestu.BuildConfig
import com.kazakimaru.ch08_ajifauzipangestu.R
import com.kazakimaru.ch08_ajifauzipangestu.activity.HomeActivity.Companion.BUNDLEEXTRA
import com.kazakimaru.ch08_ajifauzipangestu.activity.HomeActivity.Companion.DATABUNDLE
import com.kazakimaru.ch08_ajifauzipangestu.helper.viewModelsFactory
import com.kazakimaru.ch08_ajifauzipangestu.service.TMDBClient
import com.kazakimaru.ch08_ajifauzipangestu.service.TMDBApiService
import com.kazakimaru.ch08_ajifauzipangestu.viewmodel.TMDBViewModel

class MovieDetailActivity: AppCompatActivity() {
    private val apiService: TMDBApiService by lazy { TMDBClient.instance }
    private val tmdbViewModel: TMDBViewModel by viewModelsFactory { TMDBViewModel(apiService) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent { MovieDetailInterface() }

        tmdbViewModel.getDetailMovie(getDatabundle()!!)
    }

    @Composable
    fun MovieDetailInterface() {
        val myDetailMovie by tmdbViewModel.detailSuccess.observeAsState()
        val getImageMovie = myDetailMovie?.posterPath
        val getTitleMovie = myDetailMovie?.title
        val getReleaseDate = myDetailMovie?.releaseDate
        val getOverview = myDetailMovie?.overview

        val plusJakartaSansRegular = FontFamily(Font(R.font.plusjakartasansregular))
        val plusJakartaSansBold = FontFamily(Font(R.font.plusjakartasansbold))

        Column() {

            Spacer(modifier = Modifier.height(80.dp))

            Box(
                modifier = Modifier.fillMaxWidth(),
                contentAlignment = Alignment.Center
            ) {
                // Start Image Poster
                AsyncImage(
                    model = BuildConfig.BASE_URL_IMAGE + getImageMovie,
                    contentDescription = null,
                    modifier = Modifier.size(width = 160.dp, height = 220.dp),
                    contentScale = ContentScale.Crop
                )
                // End Image Poster
            }

            Column(
                modifier = Modifier.padding(vertical = 32.dp, horizontal = 16.dp)
            ) {
                // Start Title Movie
                Text(
                    text = "$getTitleMovie",
                    fontSize = 22.sp,
                    color = Color.Black,
                    fontFamily = plusJakartaSansBold
                )
                // End Title Movie

                Spacer(modifier = Modifier.height(4.dp))

                // Start Release Date
                Text(
                    text = "Release Date: $getReleaseDate",
                    fontSize = 16.sp,
                    fontFamily = plusJakartaSansRegular
                )
                // End Release Date

                Spacer(modifier = Modifier.height(20.dp))

                // Start Overview Title
                Text(
                    text = "Overview:",
                    fontSize = 18.sp,
                    color = Color.Black,
                    fontFamily = plusJakartaSansBold
                )
                // End Overview Title

                Spacer(modifier = Modifier.height(4.dp))

                // Start Overview
                Text(
                    text = "$getOverview",
                    fontSize = 18.sp,
                    color = Color.Black,
                    fontFamily = plusJakartaSansRegular
                )
                // End Overview
            }
        }
    }

    private fun getDatabundle(): Int? {
        val getBundle = intent.getBundleExtra(BUNDLEEXTRA)
        return getBundle?.getInt(DATABUNDLE)
    }
}
